##Project RestApi

#RestApi for storing the User data (Post) 
localhost:8080/user

#Json Body
{
    "firstName": "Payal",
    "lastName": "Jadhav",
    "birthDate": "2005-12-31T18:29:59.000+0000",
    "address": "Nagar"
}

#RestApi for retrieving all users data (Get)
localhost:8080/users

#RestApi for retrieving single user data using its id (Get)
localhost:8080/users/1

#RestApi for updating the user data (Put)
localhost:8080/users/1

#Json Body
{
    "firstName": "Payal",
    "lastName": "Jadhav",
    "birthDate": "2005-12-31T18:29:59",
    "address": "Pune"
}

#RestApi for retrieving a single storing person using firstName and/Or lastName
localhost:8080/user?firstName=payal&lastName=jadhav
localhost:8080/user?firstName=payal
localhost:8080/user?lastName=jadhav
localhost:8080/user?lfirstName=payal&lastName=
localhost:8080/user?lfirstName=a&lastName=jadhav