package com.springboot.rest.test;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import java.text.SimpleDateFormat;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import com.springboot.rest.SpringBootDemoApplication;
import com.springboot.rest.dao.UserRepository;
import com.springboot.rest.model.UserModel;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;



@RunWith(SpringRunner.class)
@DataJpaTest

public class UserRepoTest {

	@Test
	public void contextLoads() {
	}

	
	@Autowired
	UserRepository userRepository;
	
	

	  @Test
	  @Rollback(false)
	    public void findAllUsers() throws ParseException, java.text.ParseException {
		 
		  UserModel user1 = userRepository.save(new UserModel(1, "Shweta", "Khetre",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2005-12-31 23:59:59"), "Pune"));
		  UserModel user2 = userRepository.save(new UserModel(2, "Pooja", "Joshi",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("1995-08-19 23:59:59"), "Beed"));
	      UserModel user3 = userRepository.save(new UserModel(3, "Supriya", "Jadhav",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2000-06-26 23:59:59"), "Nagar"));																																																																																																																	
	      Iterable<UserModel> userList = userRepository.findAll();
		  assertFalse(userRepository.findAll().isEmpty());
//		  assertThat(userList).hasSize(3).contains(user1, user2,user3);

	  }
	  
	  @Test
	  @Rollback(false)
	  public void findById() throws java.text.ParseException {
		  
		  UserModel user1 = userRepository.save(new UserModel(1, "Shweta", "Khetre",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2005-12-31 23:59:59"), "Pune"));
		  UserModel user = userRepository.findByUserId(1);
		  assertThat(user.getUserId()).isEqualTo(1);
	  }

	  @Test
	  @Rollback(false)
	  public void updateById() throws java.text.ParseException {
		  
		  UserModel user1 = userRepository.save(new UserModel(1, "Shweta", "Khetre",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2005-12-31 23:59:59"), "Pune"));
		  UserModel user = userRepository.findByUserId(1);
		  user.setAddress("Chennai");
		  userRepository.save(user);
		  UserModel updateduser = userRepository.findByUserId(1);
		  assertThat(updateduser.getAddress()).isEqualTo("Chennai");

	  }
	  
	  @Test
	  @Rollback(false)
	  public void findByFirstLastName() throws java.text.ParseException{
		  
		  UserModel user1 = userRepository.save(new UserModel(1, "Shweta", "Khetre",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2005-12-31 23:59:59"), "Pune"));
		  UserModel user2 = userRepository.save(new UserModel(2, "Shilpa", "Khetre",new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2005-12-31 23:59:59"), "Beed"));
		  List<UserModel> firstAndLastName = userRepository.findByFirstNameIgnoreCaseAndLastNameIgnoreCase("Shweta", "Khetre");
		  List<UserModel> firstOrLastName = userRepository.findByFirstNameIgnoreCaseOrLastNameIgnoreCase("Shweta", "Khetre");
		  
		  if(firstAndLastName != null)
		  {
			  assertTrue(true);
		  }
		  else if(firstOrLastName != null)
		  {
			  assertTrue(true);
		  }
		  else
			  assertTrue(false);
	 
	  }
}

