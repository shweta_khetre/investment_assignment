package com.springboot.rest.controller;


import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.rest.dao.UserRepository;
import com.springboot.rest.model.*;
import com.springboot.rest.services.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Value("${spring.datasource.driverClassName}")
	private String driverClassName;
	
	@GetMapping("/test")
	public String testConnection() {
	
		Logger.getLogger("hdk  "+driverClassName);
		return driverClassName;
	}
	
	@GetMapping("/users")
	public List<UserModel> getUsers(){

		return userService.getUsers();
	
		
	}
	
	@GetMapping("/users/{userId}")
	public UserModel getUserById(@PathVariable Long userId) {
		
		return userService.getUser(userId);
				
			      
		
	}
	
	@PostMapping("/user")
	public UserModel createUser(@Valid @RequestBody UserModel user) {
		
	 return userService.createUser(user);
	 
	}
	
	@PutMapping("/users/{userId}")
	public ResponseEntity<UserModel> updateUser(@PathVariable Long userId,
	  @Valid @RequestBody UserModel userDetails) {
	
		
		UserModel updatedUser = userService.updateUser(userId, userDetails);
		return ResponseEntity.ok(updatedUser);
	}
	
	
	@GetMapping("/user")
    public List<UserModel> getUserByName(@RequestParam Map<String, String> name) {
		
        String firstName=null;
        String lastName=null;
        firstName = name.get("firstName");
        lastName = name.get("lastName");		
       																																																																
        
        return userService.searchByFirstLastName(firstName, lastName);
    }																											
		
		
	}	

