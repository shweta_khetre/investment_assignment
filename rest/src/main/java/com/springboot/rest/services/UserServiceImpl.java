package com.springboot.rest.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.springboot.rest.dao.UserRepository;
import com.springboot.rest.model.UserModel;

@Service
public class UserServiceImpl implements UserService {

	List<UserModel> userList;
	
	@Autowired
	UserRepository userRepository;

	public List<UserModel> getUsers() {
		userList = new ArrayList<UserModel>();
		userRepository.findAll().forEach(user -> userList.add(user));
		return userList;
	}



	public UserModel getUser(long userId) {
		
		return userRepository.findByUserId(userId);
	}



	@Override
	public UserModel createUser(UserModel user) {
		
		return userRepository.save(user);
	}

	

	@Override
	public UserModel updateUser(long userId, UserModel userDetails) {
		 UserModel user = userRepository.findByUserId(userId);

		 user.setFirstName(userDetails.getFirstName());
		 user.setLastName(userDetails.getLastName());
		 user.setBirthDate(userDetails.getBirthDate());
		 user.setAddress(userDetails.getAddress());
		UserModel updatedUser = userRepository.save(user);
		 return updatedUser;
	}



	@Override
	public List<UserModel> searchByFirstLastName(String firstName, String lastName) {
		
		List<UserModel> searchByfirstAndLastName = userRepository.findByFirstNameIgnoreCaseAndLastNameIgnoreCase(firstName, lastName);
		if(!searchByfirstAndLastName.isEmpty())
			return searchByfirstAndLastName;
		else
			return userRepository.findByFirstNameIgnoreCaseOrLastNameIgnoreCase(firstName, lastName);
	}																															



}
