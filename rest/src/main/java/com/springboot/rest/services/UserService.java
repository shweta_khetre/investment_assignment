package com.springboot.rest.services;

import java.util.List;

import javax.validation.Valid;

import com.springboot.rest.model.*;

public interface UserService {
	
	public List<UserModel> getUsers();

	public UserModel getUser(long userId);
	
	public UserModel createUser(UserModel user);
	
	public UserModel updateUser(long userId, UserModel userModel);
	
	public List<UserModel> searchByFirstLastName(String firstName, String lastName);


}
