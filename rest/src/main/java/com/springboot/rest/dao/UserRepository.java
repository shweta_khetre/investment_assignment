package com.springboot.rest.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.springboot.rest.model.*;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Long> {

	UserModel findByUserId(long userId);
																					
	List<UserModel> findByFirstNameIgnoreCaseAndLastNameIgnoreCase(String firstName, String lastName);
																												
	List<UserModel> findByFirstNameIgnoreCaseOrLastNameIgnoreCase(String firstName, String lastName);																								
	
	
}
